import 'dart:io';
import 'package:test/expect.dart';

import 'Diamond.dart';
import 'Game.dart';
import 'Player.dart';
import 'Enemie.dart';
import 'Heart.dart';
import 'Item.dart';
import 'dart:math';

import 'Sword.dart';
import 'Weapon.dart';


class MapGame{
  Random random = Random();
  static int width =20;
  static int height = 10;
  late Diamond diamond;
  late Player player;
  late Sword sword;
  var table = List.generate(width, (i) => List.generate(height, (j) => "-"));
  int count =0;
  String message = "";
  List<Item> allItem = [];
  List<Enemie> allEnemies = [];
  
  int win = 0;
  var fight = "";
  var action = "";
  int chooseWeap = 0;
  bool turn = true;
  
  
  MapGame(){}
  
  void setPlayer(Player player){
    this.player= player;
  }

  void setDiamond(Diamond diamond){
    this.diamond= diamond;
  }

  void setSword(Sword sword){
    this.sword=sword;
  }

  int getHeight(){
    return height;
  }

  int getWidth(){
    return width;
  }

  List getMap(){
    return table;
  }

  int getWin(){
    return win;
  }

  Player getPlyer(){
    return player;
  }

 
  Diamond getDiamond(){
    return diamond;
  }

  List getListEnemie(){
    return allEnemies;
  }
  bool inMap(int x, int y){
    return (x >= 0&& x <= (width-1))&&(y >=0 && y <= (height-1));
  }


   void addItem(Item item){
    allItem.add(item);
  }

  void addWeapon(Weapon weapon){
    player.allWeapon.add(weapon);
  }

  void addEnemie(Enemie enemie){
    allEnemies.add(enemie);
  }

   bool printIsOnSymbol(int x, int y, List<dynamic> map1) {
    for(var i = 0;i<allItem.length;i++){
        if(allItem[i].isOn(x, y)){
        switch(allItem[i].runtimeType){
          case Diamond:{
            checkWin();
             if(player.isOn(x, y)){
              showPlayer();
              return true;
            }
            stdout.write("${allItem[i].getSymbol()} ");
            return true;
          }
          case Heart:{
            if(player.isOn(x, y)){
              showPlayer();
              return true;
            }
            stdout.write("${allItem[i].getSymbol()} ");
            return true;
          }case Sword:{
            if(player.isOn(x, y)){
              showPlayer();
              return true;
            }
            stdout.write("${allItem[i].getSymbol()} ");
            return true;
          }
          default :{}
          break;
        }
        }
    }
    for(var i = 0;i<allEnemies.length;i++){
      if(allEnemies[i].isOn(x, y)){
        if(player.isOn(x, y)){
          showPlayer();
          return true;
        }
        stdout.write("${allEnemies[i].getSymbol()} ");
        return true;
      }
    }
    if(player.isOn(x, y)){
      showPlayer();
      return true;
    }
    return false;
   }

//     //แสดงแมพ
  void showMap(){
    for( var y = 0 ; y < getHeight(); y++ ) { 
      for( var x = 0 ; x < getWidth(); x++ ){
        if(printIsOnSymbol(x, y, table)){
        }else{
          stdout.write("${table[x][y]} ");
        }
      }
      print("");
    }
  }

  
  bool checkGetItem(){
    for(int i= 0;i< allItem.length;i++){
      if(player.getX()==allItem[i].getX()&&player.getY()==allItem[i].getY()){
        switch(allItem[i].runtimeType){
          case Heart :{
            Heart h = Heart(allItem[i].getX(), allItem[i].getY(), allItem[i].getSymbol());
            stdout.write("\nYou recived Heart");
            player.setHP(h.addHP());
            stdout.write("\nHP'Player is ${player.getHP()}\n");
            allItem[i].setNullItem();
            allItem.removeAt(i);
            return true;
          }case Sword:{
            Sword s = Sword(allItem[i].getX(), allItem[i].getY(), allItem[i].getSymbol());
            stdout.write("\nYou recived Sword\n");
            stdout.write("Sword's add into your Inventory.\n\n");
            player.setWeapon(s);                  //add sword to inventory
            allItem[i].setNullItem();
            allItem.removeAt(i);
            return true;
          }
       }
     } 
    }
    return false;
  }


  bool checkEnemie(){
   
    for(int i= 0;i< allEnemies.length;i++){
      if(player.getX()==allEnemies[i].getX()&&player.getY()==allEnemies[i].getY()){
        
         while(true){
          stdout.write("You Found The Enemie Lv.${allEnemies[i].getLevel()}, HP = ${allEnemies[i].getHP()} \n");
          stdout.write("You Want to fight? \n Select your choice... (Y = yes, N = No) \n");
          stdout.write("Choose action : ");
          fight = stdin.readLineSync()!;
          var level = allEnemies[i].getLevel()-player.getLevel();

          if(player.getLevel()<=allEnemies[i].getLevel()&&level >= 4){     //Check Level of Player.
            stdout.write("Your Level is too low. You need more level, you need more level...\n");
            stdout.write("Press anykey to accept :");
            var ok = stdin.readLineSync()!;
            return false;
          }
          if(fight=="Y"||fight=="y"){

            while(allEnemies[i].getHP()>0){               //Fight untill has someone have hp <= 0
            stdout.write("\nTurn Player(choose one from these for action) : 1 = Fight 2 = Escape \n Choose : ");
            action = stdin.readLineSync()!;
            switch(action){
              case "1": {
                if(player.getWeapon().isEmpty){
                  stdout.write("\nPlayer don't have any Weapon! (Better choose to escape) \n");
                  stdout.write("Press anykey to accept :");
                  var ok = stdin.readLineSync()!;
                  continue;
                }else{
                    showWeapon();
                    chooseWeap = checkEmptyOrNotInt();
                    switch(player.getWeap(chooseWeap).runtimeType){
                      case Sword: {
                        Sword s = Sword(player.getWeap(chooseWeap).getX(),player.getWeap(chooseWeap).getY(),player.getWeap(chooseWeap).getSymbol());
                        allEnemies[i].getHit(s.getAtk());
                        s.attack();
                        stdout.write(" The Enemies got hit!!! HP'Enemie left: ${allEnemies[i].getHP()} \n");
                        if(allEnemies[i].getHP()<=0){
                          print("Enemies was eliminate~");
                          stdout.write("OK!!!");
                            var z = stdin.readLineSync()!;
                          level = player.getLevel() - allEnemies[i].getLevel();
                          if(level.isNegative){
                            level.abs();
                          }else if(level.abs()==0){
                            level = 1;
                          }else if(level>0){
                            level = 1;
                          }
                          player.levelUp(level.abs());
                          allEnemies[i].setNullEnemie();
                          allEnemies.removeAt(i);
                          return true;
                        }
                      }
                    }
                    
                    stdout.write("\nTurn Enemies : ");
                    allEnemies[i].attack();
                    int pass = 0;
                    while(pass==0){
                    stdout.write("\n(Choose one from these for action) : 1 = Block, 2 = Dodge, 3 = Escape , i = Check HP's Player \n Choose : ");
                    action = stdin.readLineSync()!;
                     switch(action){
                       case "1" : {
                         int randomNumber = random.nextInt(100);
                         if(randomNumber<26){
                          player.getHit((allEnemies[i].getAtk()/2));
                          stdout.write("The Player choose to block the attack.\n");
                          stdout.write("The Player block failed.\n");
                          stdout.write("The Player got hit!!! HP'Player left: ${player.getHP()} \n");
                         }else{
                          stdout.write("The Player choose to block the attack. Block success Player have chance to counter!!!\n");
                          Sword s = Sword(player.getWeap(chooseWeap).getX(),player.getWeap(chooseWeap).getY(),player.getWeap(chooseWeap).getSymbol());
                          s.attack();
                          allEnemies[i].getHit(s.getAtk());
                          stdout.write(" The Enemies got hit!!! HP'Enemie left: ${allEnemies[i].getHP()} \n");
                          if(allEnemies[i].getHP()<=0){
                            print("Enemies was eliminate~");
                            stdout.write("OK!!!");
                            var z = stdin.readLineSync()!;
                            level = player.getLevel() - allEnemies[i].getLevel();
                            if(level.isNegative){
                              level.abs();
                            }else if(level.abs()==0){
                              level = 1;
                            }else if(level>0){
                              level = 1;
                           }
                          player.levelUp(level.abs());
                          allEnemies[i].setNullEnemie();
                          allEnemies.removeAt(i);
                          return true;
                          }
                         }
                         if(checkLive()){
                            stdout.write("Player are dead!!!");
                            return true;
                          }
                         pass++;
                         break;
                       }case "2":{
                          stdout.write("Player choose dodge. Playerdidn't get any attack~");
                          pass++;
                          break;
                       }
                       case "3":{
                          stdout.write("Player choose to escape...");
                          return false;
                       }case "i":{
                          stdout.write("HP's Player is ${player.getHP()}");
                          continue;
                       }
                       default :{
                         continue;
                      }
                      }
                    }
                  
                }
                break;
              }case "2":{
               stdout.write("You choose to escape...");
               return false;
              }
              default :{break;}
            }
            // stdout.write("${allEnemies[i].toString()}");
            //  if(allEnemies[i].getHP()<=0){
            //    allItem[i].setNullItem();
            //    allItem.removeAt(i);
            //    return true;
            //  }
            // return true;
          }

         }else if(fight=="N"||fight=="n"){
          stdout.write("You choose to not fight...");
          return false;
         }else{
            continue;    
         }
          if(allEnemies[i].getHP()<=0){
          allItem[i].setNullItem();
          allItem.removeAt(i);
          return true;
         }
        }
     } 
    }
    return true;
  }

  int checkEmptyOrNotInt() {
    while(true){
    stdout.write("\nChoose (Number)Your Weapon to attack : ");
    try {
      var checkChoose = stdin.readLineSync()!;
      int x = int.parse(checkChoose);
      if(x<=0){
        continue;
      }if((x-1)>=player.getWeapon().length){
        continue;
      }else{
        return x-1;
      }
    } catch (e) {
      continue;
    }
    }
  }


  bool checkWin() {
    if((player.getX()==diamond.getX())&&(player.getY()==diamond.getY())){
        win++;
     }
    if(win>0){  //เช็คว่าชนะหรือยัง?
      win=0;
      return true;
    }
    return false;
  }
    
void showWeapon(){
  stdout.write("Your inventory : \n");
    if(player.getWeapon().isEmpty){
        stdout.write("Empty...\n");
      }
    for(int i =0; i<player.getWeapon().length;i++){  
      stdout.write("${i+1}. ${player.getWeap(i).toString()} \n");
     }
}

 void showPlayer(){
    stdout.write("${player.getSymbol()} ");
  }

  bool checkLive(){
    if(player.getHP()<=0){
      return true;
    }
    return false;
  }

}