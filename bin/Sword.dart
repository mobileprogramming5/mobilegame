
import 'Attacking.dart';
import 'MapGame.dart';
import 'Item.dart';
import 'Weapon.dart';
import 'Player.dart';

import 'dart:io';

class Sword extends Weapon with Attacking{

  int atk = 100;
  Sword(int x,int y, String symbol) : super(x, y, 'S');

 void setAtk(int atk){
    this.atk = atk;
  }

  
  @override
  void attack() {
    stdout.write("Player use Sword to Attack!!!");
  }

  @override
  String toString() {
    return "Sword : atk = $atk";
  }

}