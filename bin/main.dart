import 'package:dart_application_1/dart_application_1.dart' as dart_application_1;
import 'Diamond.dart';
import 'Enemie.dart';
import 'Game.dart';
import 'Heart.dart';
import 'MapGame.dart';
import 'Player.dart';
import 'Item.dart';
import 'dart:io';

import 'Sword.dart';

void main() {
  int checkKey = 0;
  int checkEndGame =0;
  Game game= Game();


 
  while(checkEndGame==0){
  Player p = Player(0, 0,"");
  Diamond diamond = Diamond(19, 9, "D");
  print("Press Enter to play game ...");
  var x= stdin.readLineSync()!;
  mockUpMap(game, p, diamond);
    game.clearScreen();
    game.showWelcome();
    game.showMenu();
    game.showMap();
    while(true){
    stdout.write("\nInput Direction: ");
    String direction  = inputDirection();
    checkKey= checkSpecialKey(direction, p);
    switch (checkKey){
      case 1 :{
        return; 
      }case 2 :{
        game.clearScreen();
        game.showPlayer();
        continue;
      }case 3 :{
        game.clearScreen();
        game.showInventory();
        continue;
      }case 4 :{
        game.setShowMap();
        continue;
      }default:{
        break;
      }
    }

    p.move(direction);
    if(!game.checkAction()){
      checkEnemieNotdieOrEscape(direction, p);
    }else{
      if(game.checkPlayerIsDead()){
        checkEndGame++;
        break;
      }
    }

    if(game.checkFoundItem()){
      continue;
    }
    
    game.setShowMap();

    if(game.showWin()){
      checkEndGame++;
      break;
    }
  }
  if(checkEndGame>0){
    stdout.write("\n Do u want to play again? \n Yes = Y, No = input any : ");
      var p = stdin.readLineSync()!;
      if(p==("Y")||p==("y")){
        checkEndGame=0;
      }
    }
  }
}

void mockUpMap(Game game, Player p, Diamond diamond) {
  game.setMap(MapGame());
  game.setPlayer(p);           //important
  game.addItem(diamond);       //important
  
  //Add Heart 
  game.addItem(Heart(2, 0, "H"));
  game.addItem(Heart(4, 3, "H"));
  game.addItem(Heart(4, 8, "H"));
  game.addItem(Heart(0, 5, "H"));
  game.addItem(Heart(17, 5, "H"));
  game.addItem(Heart(18, 7, "H"));
  game.addItem(Heart(16, 2, "H"));
  game.addItem(Heart(10, 0, "H"));
  
  //Add Weapon
  game.addItem(Sword(0, 1, ""));
  
  //Add Enemie
  game.addEnemie(Enemie(8, 3, "M", 400, 5));
  game.addEnemie(Enemie(0, 4, "M", 400, 2));
  game.addEnemie(Enemie(1, 5, "M", 450, 3));
  game.addEnemie(Enemie(18, 0, "M", 450, 9));
  game.addEnemie(Enemie(18, 1, "M", 450, 10));
  game.addEnemie(Enemie(17, 2, "M", 450, 10));
  game.addEnemie(Enemie(8, 2, "M", 100, 2));
  game.addEnemie(Enemie(8, 1, "M", 200, 2));
  game.addEnemie(Enemie(19, 8, "M", 500, 14));
  game.addEnemie(Enemie(18, 8, "M", 800, 13));
  game.addEnemie(Enemie(18, 9, "M", 650, 15));
}
  
  

void checkEnemieNotdieOrEscape(String direction, Player p) {
  switch (direction) {
  case 'N':
  case 'w':
    p.setY(p.getY()+1);
    break;
  case 'S':
  case 's':
    p.setY(p.getY()-1);
    break;
  case 'E':
  case 'd':
    p.setX(p.getX()-1);
    break;
  case 'W':
  case 'a':
    p.setX(p.getX()+1);
    break;
      }
}

int checkSpecialKey(String direction, Player p) {
  if(direction=="Q" || direction=="q"){
    return 1;
  }else if(direction =="C" || direction =="c"){
    // if(p.getSword().){}
    return 2;
  }else if(direction =="I" || direction =="i"){
     return 3;
  }else if(direction == "M"||direction == "m"){
    return 4;
  }
  return 0;
}
String inputDirection(){
  String input = stdin.readLineSync()!;
  return input;
}  
