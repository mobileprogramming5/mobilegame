import 'MapGame.dart';
import 'Heart.dart';
import 'Item.dart';
import 'Weapon.dart';
import 'Diamond.dart';
import 'Sword.dart';


class Player {
  late int x;
  late int y;
  String symbol = "P";
  String name = "";
  double hp = 100;
  int level = 1;
  MapGame map = MapGame();
  List<dynamic> allWeapon = [];
  Player(int x, int y, String s) {
    this.x = x;
    this.y=y;
  }  

  int getX() {
    return x;
  }

  int getY() {
    return y;
  }

  String getName() {
    return name;
  }

  double getHP() {
    return hp;
  }

  int getLevel() {
    return level;
  }

  String getSymbol() {
    return symbol;
  }


  void setX(int x){
    this.x = x;
  }

  void setY(int y){
    this.y = y;
  }

  void setHP(var hp){
    this.hp += hp; 
  }

  void getHit(var atk){
    hp-=atk;
  }

  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }

  void addHeart(Heart heart) {
    hp += heart.addHP();
  }

  void setWeapon(Weapon weapon){
    allWeapon.add(weapon);
  }

  List getWeapon(){
    return allWeapon;
  }

  Weapon getWeap(int i){
    allWeapon[i];
    return allWeapon[i];
  }

  void levelUp(int level){
    this.level += level;
  }
  

  bool move(direction) {
    switch (direction) {
      case 'N':
      case 'w':
        if (moveN()) return false;
        break;
      case 'S':
      case 's':
        if (moveS()) return false;
        break;
      case 'E':
      case 'd':
        if (moveE()) return false;
        break;
      case 'W':
      case 'a':
        if (moveW()) return false;
        break;
    }
    return true;
  }

  bool moveW() {
    //move to west direction
    if (map.inMap(x - 1, y)) {
      x = x - 1;
    } else {
      return true;
    }
    return false;
  }

  bool moveE() {
    //move to east direction
    if (map.inMap(x + 1, y)) {
      x = x + 1;
    } else {
      return true;
    }
    return false;
  }

  bool moveS() {
    //move to south direction
    if (map.inMap(x, y + 1)) {
      y = y + 1;
    } else {
      return true;
    }
    return false;
  }

  bool moveN() {
    //move to north direction
    if (map.inMap(x, y - 1)) {
      y = y - 1;
    } else {
      return true;
    }
    return false;
  }

  @override
  String toString() {
    // TODO: implement toString
    return "player + $x + $y";
  }
}
