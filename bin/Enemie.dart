import 'dart:io';
import 'Attacking.dart';
import 'Player.dart';

class Enemie extends Player with Attacking{
  int atk = 100;
  Enemie(int x,int y,String symbol,double hp,int level) : super(x,y,symbol){
    this.x = x;
    this.y = y;
    this.symbol = symbol;
    this.hp=hp;
    this.level = level;
  }

  int getAtk(){
    return atk;
  }
@override
  void attack(){
    stdout.write("Enemie choose Crawl!!");
  }


//   void hitPlayer(Player p){
//     p.hp -=50;
//   }
  
  @override
  String toString() {
    // TODO: implement toString
    return "Enemies : Lv. $level, $x  ,$y";
  }

  void setNullEnemie() {
    symbol = "-";
  }
}