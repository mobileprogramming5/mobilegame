import 'MapGame.dart';

abstract class Item{
  late int x;
  late int y;
  late String symbol;

  Item(int x,int y,String symbol){
    this.x=x;
    this.y=y;
    this.symbol=symbol;
  }



  int getX(){
    return x;
  }

  int getY(){
    return y;
  }

  void setNullItem(){
    this.symbol = "-";
  }

  String getSymbol(){
    return symbol;
  }
  
  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }

  void setX(int x){
    this.x= x;
  }

  void setY(int y){
    this.y= y;
  }

  @override
  String toString() {
    // TODO: implement toString
    return "Item : x =  ${x} , y= $y , symbol = $symbol  ";
  }
}