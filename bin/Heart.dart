import 'Item.dart';

class Heart extends Item{
  double hp = 100;
  Heart(int x,int y,String symbol) : super(x, y, symbol="H"){}

  double addHP(){
    return hp;
  }
  
}