import 'dart:io';
import 'dart:math';
import 'Diamond.dart';
import 'Player.dart';
import 'MapGame.dart';
import 'Item.dart';
import 'Heart.dart';
import 'Enemie.dart';
import 'Sword.dart';
import 'Weapon.dart';

class Game{

  late MapGame map ;
  late Player player;
  int count =0;
  bool status = false;
  Game(){}
  
  void showWelcome(){
    print("Welcome to Steal the Diamonds");
  }

  void showLose(){
    print("You lose...");
  }

  void showMap(){
   map.showMap();
 }

 void setPlayer(Player player){
    this.player = player;
    this.map.setPlayer(player);
  }

  void setMap(MapGame map){
    this.map = map;
  }

  void addItem(Item item){
    map.addItem(item);
    if(item.runtimeType==Diamond){
      map.setDiamond(Diamond(item.getX(),item.getY(), item.getSymbol()));
    }
  }

  void addEnemie(Enemie enemie){
    map.addEnemie(enemie);
  }

 void setStatus(){
  status = false;
 }

 bool checkAction(){
  if(map.checkEnemie()){
    return true;
  }
  return false;
 }

 bool checkFoundItem(){
  if(map.checkGetItem()){
    return true;
  }
  return false;
 }

  bool checkPlayerIsDead(){
    if(map.checkLive()){
      stdout.write("Game Over");
      return true;
    }
    return false;
  }

  bool showWin(){
    if(map.checkWin()){
      print("\nYou got THE Daimond!!! You Win");
      return true;
    }
    return false;
  }
  
  void showMenu(){
    stdout.write("Descibe Object on Map : ");
    stdout.write("Player = ${map.getPlyer().getSymbol()}, H = Heart, Diamonnd = ${map.getDiamond().getSymbol()}," );
    stdout.write(" Enemie = M, S = Sword(Weapon) \n\n");
    stdout.write("Menu : (Press one input from these to perform) ");
    stdout.write(" Q or q to Quite from Game , C or c to Check Status Player, I or i to Check inventory, M or m to Show map.\n");
  }

  void showInventory(){
    map.showWeapon();
  }

  void showPlayer(){
    stdout.write("Your Level is ${player.getLevel()} HP is ${player.getHP()}");
  }

  void clearScreen(){
    print("\x1B[2J\x1B[0;0H");
  }

  void setShowMap(){
    clearScreen();
    print("");
    showMenu();
    print("");
    showMap();
  }
}