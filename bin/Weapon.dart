import 'Attacking.dart';
import 'Item.dart';
import 'dart:io';

abstract class Weapon extends Item {
  late int atk;
  Weapon(int x,int y,String symbol) : super(x, y, symbol);

  int getAtk(){
    return atk;
  }

  @override
  String toString() {
    // TODO: implement toString
    return "Weapon : x =  $x , y= $y , symbol = $symbol  ";
  }
}